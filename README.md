# chatMessageContextDelete
Adds an option to the chat message context menu to delete.

![scene context menu with Delete option](img/chatMessageContextDelete.png)

# Manifest

[https://gitlab.com/reichler/chatMessageContextDelete/-/raw/master/chatMessageContextDelete/module.json](https://gitlab.com/reichler/chatMessageContextDelete/-/raw/master/chatMessageContextDelete/module.json)
