Hooks.on('getChatLogEntryContext', (html, options) => {
    let viewOption = {
        name: "Delete",
        icon: '<i class="fas fa-trash" style="color:#ff0000;"></i>',
        condition: li => { return true },
        callback: li => {
            const message = game.messages.get(li.data("messageId"));
            message.delete();
        }
    };
    options.push(viewOption);
});
